<?php

namespace PHorum\DAO;

require_once("../BD/connection.php");
require_once("../Entity/CategorieEntity.php");

class CategorieDAO{

  //-------------------------CREATE-----------------
  /**
  * @param string$titre
  * @return int$id
  */
  public static function createCategorie(string $titre){
    $sql="INSERT INTO categorie (titre) VALUES (?)";
    $connexion = Connect::getConnexion();
    $query = $connexion->prepare($sql);
    $query->bindParam(1,$titre,PDO::PARAM_STR,60);
    $res = $query->execute();

    if(!$res)die("CategorieDAO : createCategorie : erreur lors de la creation de la catgéorie ayant pour titre ".$titre);

    return $connexion->lastInsertId();
  }

  //--------------------DELETE---------------------
  /**
  * @param int$id
  */
  public static function deleteById(int $id){
    $sql="DELETE FROM categorie where id=?";
    $query = Connect::getConnexion()->prepare($sql);
    $query->bindParam(1,$id,PDO::PARAM_INT);
    $res = $query->execute();

    if(!$res)die("Categorie : deleteById : erreur lors de la suppression de la categorie ayant pour id ".$id);
  }

  /**
  * @param string$titre
  */
  public static function deleteByTitre(string $titre){
    $sql="DELETE FROM categorie where titre=?";
    $query = Connect::getConnexion()->prepare($sql);
    $query->bindParam(1,$titre,PDO::PARAM_STR,60);
    $res = $query->execute();

    if(!$res)die("CategorieDAO : deleteByTitre : erreur lors de la suppression de la catégorie ayant pour titre ".$titre);
  }

  //-----------------------------------UPDATE-----------------------
  /**
  * @param string$old_titre
  * @param string$new_titre
  */
  public static function updateCategorieTitre(string $old_titre, string $new_titre){
    $sql="UPDATE categorie SET titre=? where titre=?";
    $query = Connect::getConnexion()->prepare($sql);
    $query->bindParam(1,$new_titre,PDO::PARAM_STR,60);
    $query->bindParam(2,$old_titre,PDO::PARAM_STR,60);
    $res = $query->execute();

    if(!$res)die("CategoriDAO : updateCategorieTitre : erreur lors de la mise à jour de la categorie ayant pour titre ".$old_titre);
  }

  //------------------------------------READ---------------------------
  /**
  * @return array$categories
  */
  public static function getAllCategories(){
    $sql="SELECT * FROM categorie";
    $query = Connect::getConnexion()->prepare($sql);
    $res = $query->execute();

    if(!$res)die("CategoriDAO : getAllCategories : erreur lors de la mise la recuperation de toutes les categories");

    $categories = array();
    foreach($query as $categorie){
      $categories[$categorie["id"]] = new CategorieEntity(array(
        "id" => $categorie["id"],
        "titre" => $categorie["titre"]
      ));
    }
    return $categories;
  }

  /**
  * @param int$id
  * @return CategorieEntity$categorie
  */
  public static function getCategorieById(int $id){
    $sql="SELECT * FROM categorie where id=?";
    $query = Connect::getConnexion()->prepare($sql);
    $query->bindParam(1,$id,PDO::PARAM_INT);
    $res = $query->execute();

    if(!$res)die("CategorieDAO : getCategorieById : erreur lors de la recuperation de la categorie ayant pour id ".$id);

    $categorie = null;
    foreach($query as $c){
      $categorie = new CategorieEntity(array(
        "id" => $c["id"],
        "titre" => $c["titre"]
      ));
    }
    return $categorie;
  }

  /**
  * @param string$titre
  * @return CategorieEntity$categorie
  */
  public static function getCategorieByTitre(string $titre){
    $sql="SELECT * FROM categorie where titre=?";
    $query = Connect::getConnexion()->prepare($sql);
    $query->bindParam(1,$titre,PDO::PARAM_STR,60);
    $res = $query->execute();

    if(!$res)die("CategorieDAO : getCategorieTitre : erreur lors de la recuperation de la categorie ayant pour titre ".$titre);

    $categorie = null;
    foreach($query as $c){
      $categorie = new CategorieEntity(array(
        "id" => $c["id"],
        "titre" => $c["titre"]
      ));
    }
    return $categorie;
  }
}
 ?>
