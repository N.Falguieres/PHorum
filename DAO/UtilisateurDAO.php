<?php

namespace PHorum\DAO;

//----------------Import-----------------
require_once("../BD/connection.php");
require_once("../Entity/UtilisateurEntity.php");

/**
* Data Access Object des utilisateurs
*/
class UtilisateurDAO{

  //------------READ------------

  /**
  * @return array$utilisateurs tableau d'utilisateur
  */
  public static function getAllUtilisateurs(){
    $sql = "SELECT * from utilisateur";
    $query = Connect::getConnexion()->prepare($sql);
    $res = $query->execute();

    if(!$res)die("UtilisateurDAO : getAllUtilisateurs : erreur lors de la recuperation des utilisateurs");

    $users = array();
    foreach($query as $user){
      $users[$user["id"]]=new UtilisateurEntity(array(
        "id" => $user["id"],
        "pseudo" => $user["pseudo"],
        "mail" => $user["email"],
        "type" => $user["type"]
      ));
    }
    return $users;
  }

  /**
  * @param int$id
  * @return UtilisateurEntity$utilisateur
  */
  public static function getUtilisateurById(int $id){
    $sql = "SELECT * from utilisateur where id=?";
    $query = Connect::getConnexion()->prepare($sql);
    $query->bindParam(1,$id,PDO::PARAM_INT);
    $res = $query->execute();

    if(!$res)die("UtilisateurDAO : getUtilisateurById : erreur lors de la recuperation de l'utilisateur d'on l'id est ".$id);

    $user = null;
    foreach($query as $u){
      $user = new UtilisateurEntity(array(
        "id" => $u["id"],
        "pseudo" => $u["pseudo"],
        "mail" => $u["email"],
        "type" => $u["type"]
      ));
    }

    return $user;
  }

  /**
  * @param string$pseudo
  * @return UtilisateurEntity$Utilisateur utilisateur qui possède ce pseudo
  */
  public static function getUtilisateurByPseudo(string $pseudo){
    $sql = "SELECT * from utilisateur where pseudo=?";
    $query = Connect::getConnexion()->prepare($sql);
    $query->bindParam(1,$pseudo,PDO::PARAM_STR,20);
    $res = $query->execute();

    if(!$res)die("UtilisateurDAO : getUtilisateurByPseudo : erreur lors de la recuperation de l'utilisateur d'on le pseudo est ".$pseudo);

    $user = null;
    foreach($query as $u){
      $user = new UtilisateurEntity(array(
        "id" => $u["id"],
        "pseudo" => $u["pseudo"],
        "mail" => $u["email"],
        "type" => $u["type"]
      ));
    }

    return $user;
  }

  /**
  * @param string$mail
  * @return UtilisateurEntity$Utilisateur utilisateur qui possède ce mail
  */
  public static function getUtilisateurByMail(string $mail){
    $sql = "SELECT * from utilisateur where email=?";
    $query = Connect::getConnexion()->prepare($sql);
    $query->bindParam(1,$mail,PDO::PARAM_STR,50);
    $res = $query->execute();

    if(!$res)die("UtilisateurDAO : getUtilisateurByMail : erreur lors de la recuperation de l'utilisateur d'on le mail est ".$mail);

    $user = null;
    foreach($query as $u){
      $user = new UtilisateurEntity(array(
        "id" => $u["id"],
        "pseudo" => $u["pseudo"],
        "mail" => $u["email"],
        "type" => $u["type"]
      ));
    }

    return $user;
  }

  /**
  * @param int$type
  * @return array$utilisateurs liste des utilisateurs qui ont se type
  */
  public static function getUtilisateursByType(int $type){
    $sql = "SELECT * from utilisateur where type=?";
    $query = Connect::getConnexion()->prepare($sql);
    $query->bindParam(1,$type,PDO::PARAM_STR,10);
    $res = $query->execute();

    if(!$res)di("UtilisateurDAO : getUtilisateurByType : erreur lors de la recuperation des utilisateurs d'on le type est ".$type);

    $users = array();
    foreach($query as $user){
      $users[$user["id"]] = new UtilisateurEntity(array(
        "id" => $user["id"],
        "pseudo" => $user["pseudo"],
        "mail" => $user["email"],
        "type" => $user["type"]
      ));
    }
    return $users;
  }

  //---------------------CREATE----------------
  /**
  * @param string$pseudo
  * @param string$mail
  * @param int$type
  * @return int$id
  */
  public static function createUtilisateur(string $pseudo, string $mail, int $type){
    $sql = "INSERT INTO utilisateur (pseudo,email,type) VALUES (?,?,?)";
    $connexion = Connect::getConnexion();
    $query = $connexion->prepare($sql);

    $query->bindParam(1,$pseudo,PDO::PARAM_STR,60);
    $query->bindParam(2,$mail,PDO::PARAM_STR,50);
    $query->bindParam(3,$type,PDO::PARAM_INT);

    $res = $query->execute();

    if(!$res)die("UtilisateurDAO : createUtilisateur : erreur lors de la creation de l'utilisateur avec pour pseudo : "
                  .$pseudo."<br>pour email : ".$mail."<br>pour type :".$type);

    return $connexion->lastInsertId();
  }

  //--------------------UPDATE----------------
  /**
  * @param int$id
  * @param string$new_mail
  */
  public static function updateUtilisateurMail(int $id, string $new_mail){
    $sql = "UPDATE utilisateur SET email=? where id=?";
    $query = Connect::getConnexion()->prepare($sql);

    $query->bindParam(1,$new_mail,PDO::PARAM_STR,50);
    $query->bindParam(2,$id,PDO::PARAM_INT);

    $res = $query->execute();
    if(!$res)die("UtilisateurDAO : updateUtilisateurMail : erreur lors de la mise à jour de l'utilisateur ayant pour id : ".$id." avec comme nouveau mail : ".$new_mail);
  }

  /**
  * @param int$id
  * @param string$new_pseudo
  */
  public static function updateUtilisateurPseudo(int $id, string $new_pseudo){
    $sql = "UPDATE utilisateur SET pseudo=? where id=?";
    $query = Connect::getConnexion()->prepare($sql);

    $query->bindParam(1,$new_pseudo,PDO::PARAM_STR,20);
    $query->bindParam(2,$id,PDO::PARAM_INT);

    $res = $query->execute();
    if(!$res)die("UtilisateurDAO : updateUtilisateurPseudo : erreur lors de la mise à jour de l'utilisateur ayant pour id : ".$id." avec comme nouveau speudo :".$new_pseudo);
  }

  /**
  * @param int$id
  * @param int$new_type
  */
  public static function updateUtilisateurType(int $id, int $new_type){
    $sql = "UPDATE utilisateur SET type=? where id=?";
    $query = Connect::getConnexion()->prepare($sql);

    $query->bindParam(1,$new_type,PDO::PARAM_INT);
    $query->bindParam(2,$id,PDO::PARAM_INT);

    $res = $query->execute();
    if(!$res)die("UtilisateurDAO : updateUtilisateurType : erreur lors de la mise à jour de l'utilisateur ayant pour id : ".$id." avec comme nouveau type : ".$type);

  }

  //-----------------------DELETE-------------------
  /**
  * @param int$id
  */
  public static function deleteUtilisateurById(int $id){
    $sql = "DELETE FROM utilisateur where id=?";
    $query = Connect::getConnexion()->prepare($sql);

    $query->bindParam(1,$id,PDO::PARAM_INT);

    $res = $query->execute();
    if(!$res)die("UtilisateurDAO : deleteUtilisateurById : erreur lors de la suppression de l'utilisateur ayant pour id : ".$id);
  }

  /**
  *@param string$pseudo
  */
  public static function deleteUtilisateurByPseudo(string $pseudo){
    $sql = "DELETE FROM utilisateur where pseudo=?";
    $query = Connect::getConnexion()->prepare($sql);

    $query->bindParam(1,$pseudo,PDO::PARAM_STR,30);

    $res = $query->execute();
    if(!$res)die("UtilisateurDAO : deleteUtilisateurByPseudo : erreur lors de le suppression de l'utilisateur ayant pour speudo : ".$pseudo);
  }

  /**
  *@param string$mail
  */
  public static function deleteUtilisateurByMail(string $mail){
    $sql = "DELETE FROM utilisateur where email=?";
    $query = Connect::getConnexion()->prepare($sql);

    $query->bindParam(1,$mail,PDO::PARAM_STR,60);

    $res = $query->execute();
    if(!$res)die("UtilisateurDAO : deleteUtilisateurByMail : erreur lors de la suppression de l'utilisateur ayant pour mail : ".$mail);
  }


}
 ?>
