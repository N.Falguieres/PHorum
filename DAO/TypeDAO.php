<?php

 namespace PHorum\DAO;
 
require_once("../BD/connection.php");
require_once("../Entity/TypeEntity.php");

/**
* Data access object de la table type de la base de données forum
*/
class TypeDAO{


  //-----------------------------------READ----------------------------------
  /**
  *@return array$types
  */
  public static function getAllTypes(){
    $sql = "SELECT * from type";
    $query = Connect::getConnexion()->prepare($sql);
    $res = $query->execute();

    if(!$res)die("TypeDAO : getAllTypes : erreur lors de la recuperation des types");

    $types = array();
    foreach($query as $type){
      $types[$type["id"]]=new TypeEntity(array(
        "id" => $type["id"],
        "type" => $type["type"]
      ));
    }
    return $types;
  }

  /**
  *@param int$id
  *@return array$types
  */
  public function getTypeById(int $id){
    $sql="SELECT * from type where id=?";
    $query = Connect::getConnexion()->prepare($sql);
    $query->bindParam(1,$id,PDO::PARAM_INT);
    $res = $query->execute();

    if(!$res)die("TypeDAO : getTypeById : erreur lors de la recuperation du type ayant pour id : ".$id);

    $type = null;
    foreach($query as $t){
      $type = new TypeEntity(array(
        "id" => $t["id"],
        "type" => $t["type"]
      ));
    }
    return $type;
  }

  /**
  *@param string$type
  *@return TypeEntity$type
  */
  public function getTypeByType(string $type){
    $sql="SELECT * from type where type=?";
    $query = Connect::getConnexion()->prepare($sql);
    $query->bindParam(1,$type,PDO::PARAM_STR,30);
    $res = $query->execute();

    if(!$res)die("TypeDAO : getTypeByType : erreur lors de la recuperation du type ayant pour type : ".$type);

    $type = null;
    foreach($query as $t){
      $type = new TypeEntity(array(
        "id" => $t["id"],
        "type" => $t["type"]
      ));
    }
    return $type;
  }

  //------------------------------CREATE------------------------------
  /**
  *@param string$type
  * @return int$id
  */
  public function createType(string $type){
    $sql="INSERT INTO type (type) VALUES (?)";
    $connexion =  Connect::getConnexion();
    $query = $connexion->prepare($sql);
    $query->bindParam(1,$type,PDO::PARAM_STR,30);
    $res = $query->execute();

    if(!$res)die("TypeDAO : createType : erreur lors de la creation du type : ".$type);

    return $connexion->lastInsertId();
  }

  //----------------------DELETE------------------------
  /**
  *@param int$id
  */
  public function deleteTypeById(int $id){
    $sql="DELETE FROM type where id=?";
    $query = Connect::getConnexion()->prepare($sql);
    $query->bindParam(1,$id,PDO::PARAM_INT);
    $res = $query->execute();

    if(!$res)die("TypeDAO : deleteTypeById : erreu lors de la suppresion du type ayant pour id : ".$id);
  }

  /**
  *@param string$type
  */
  public function deleteTypeByType(string $type){
    $sql="DELETE FROM type where type=?";
    $query = Connect::getConnexion()->prepare($sql);
    $query->bindParam(1,$type,PDO::PARAM_STR,30);
    $res = $query->execute();

    if(!$res)die("TypeDAO : deleteTypeByType : erreur lors de la suppresionn du type : ".$type);
  }

  //------------------------------UPDATE------------------------
  /**
  *@param int$id
  *@param string$new_type
  */
  public function updateType(int $id,string $new_type){
    $sql="UPDATE type SET type=? where id=?";
    $query = Connect::getConnexion()->prepare($sql);
    $query->bindParam(1,$new_type,PDO::PARAM_STR,30);
    $query->bindParam(2,$id,PDO::PARAM_INT);
    $res = $query->execute();

    if(!$res)die("TypeDAO : updateType : erreur lors de la modification du type ayant pour id : ".$id." avec la nouvelle valeur : ".$type);
  }
}
 ?>
