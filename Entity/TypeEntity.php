<?php
namespace PHorum\Entity;
require_once("../Utils/SetUtils.php");

/**
* represente la table type de la base de données
*/
class TypeEntity{
  use SetUtils;

  //-------------------CHAMP PRIVE------------------
  private $_id;
  private $_type;

  public function __construct(array $donnees){
    $this->setUtils($donnees);
  }

  //------------------------GETTER-----------------
  /**
  * @return integer$id
  */
  public function getId(){
    return $this->_id;
  }

  /**
  *@return string$type
  */
  public function getType(){
    return $this->_type;
  }

  //---------------------SETTER-----------------------
  /**
  *@param integer$id
  */
  private function setId($id){
    $this->_id = $id;
  }

  /**
  *@param string$type
  */
  private function setType($type){
    $this->_type = $type;
  }
}
 ?>
