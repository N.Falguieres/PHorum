<?php

namespace PHorum\Entity;

/**
* class representant la table categorie
*/
class CategorieEntity{

  use setUtils;

  private $_id;
  private $_titre;

  /**
  * @param int$id
  * @param string$titre
  */
  public function __construct(array $donnees){
    $this->setUtils($donnees);
  }

  //------------------------GETTER---------------
  /**
  * @return int$id
  */
  public function getId(){
    return $this->_id;
  }

  /**
  * @return string$titre
  */
  public function getTitre(){
    return $this->_titre;
  }

  //------------------------SETTER-------------------
  /**
  * @param int$id
  */
  private function setId(int $id){
    $this->_id;
  }

  /**
  * @param string$titre
  */
  private function setTitre(string $titre){
    $this->_titre;
  }

}
?>
