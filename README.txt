Projet PHP dans le cadre de la Licence Professionnel dev web et mobile a l'IUT d'Orl�ans.
Developper un Forum en php.

Installation de la base de données :
  - Modifier les informations user, password, server et nom de la base dans "BD/config.php"
  - Créer la base "forum" dans phpMyAdmin
  - copier/coller la génération des tables de init.sql dans phpMyAdmin

  Installation des packages avec composer :
    - composer install

Adresse de l'application : http://localhost/~lhure/PHorum/www
